// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika

var pomoc = [];
var poslano = true;

var Klepet = function(socket) {
  this.socket = socket;
};

function dodajSlike(vhodnoBesedilo) {
  var arr = vhodnoBesedilo.split(" ");
  for(var beseda in arr){
  var s = arr[beseda].substring(arr[beseda].length -4, arr[beseda].length);
  if((s == ".png" | s == ".gif" | s == ".jpg") & arr[beseda].substring(0,4) == "http"){
      arr[arr.length] = " <br><img src="+'"'+arr[beseda]+'"'+"style=width:200px;height:200px"+ " hspace= "+'"'+"20px;"+'"'+ "></br>";
    }
  }
  vhodnoBesedilo = arr.join(" ");
  return vhodnoBesedilo;
}

/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      poslano=true;
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      //alert(parametri);
      if (parametri) {
        var preglej = dodajSlike(parametri[3]);
        for(var i in pomoc){
          //alert(pomoc[i][0]);
          //alert(pomoc[i][1]);
          if(pomoc[i][1]==parametri[1]){
            this.socket.emit('sporocilo', {vzdevek: pomoc[i][0], besedilo: preglej});
            sporocilo = '(zasebno za ' + pomoc[i][1]+" ("+pomoc[i][0]+")" + '): ' + preglej;
            poslano=false;
          }
        }
        if(poslano){
            this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: preglej});
            //sporocilo = '(zasebno za ' + pomoc[i][1]+"("+pomoc[i][0]+")" + '): ' + parametri[3];
            sporocilo = '(zasebno za ' + parametri[1]+'): ' + preglej;
        }
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'preimenuj':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if(parametri){
        pomoc.push([parametri[1],parametri[3]]);
        sporocilo = "cetrta "+parametri[1] +" | "+parametri[3] ;    
      }else{
        sporocilo = 'Neznan ukaz';
      }
      break;
      default:
    case 'barva':
      
      var zahtevanaBarva = besede[1];
      
      if(zahtevanaBarva){
        var trenutnaBarva = document.getElementById("kanal").style.backgroundColor
        document.querySelector("#kanal").style.backgroundColor = besede[1];
        document.querySelector("#sporocila").style.backgroundColor = besede[1];
        var novaBarva = document.getElementById("kanal").style.backgroundColor
        if(trenutnaBarva==novaBarva){
          sporocilo = 'Neznana/enaka barva';
        }
        
      }else{
        sporocilo = 'Neznan ukaz.'
      }
      break;
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};